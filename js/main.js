// carousel
$(document).ready(function(){

  $('.ui.dropdown')
	.dropdown({
    on: 'hover'
    // apiSettings: {
    //   url: '/api.semantic-ui.com/tags/{query}'
    // }
  });

  $('#login-modal')
    .modal('attach events', '#login-button', 'show')
  ;
  
  $('#sign-in-modal')
    .modal('attach events', '#sign-in-button', 'show')
  ;

  $('#cart-modal')
    .modal('attach events', '#cart-button', 'show')
  ;
  
  $('.ui.form')
    .form({
      fields: {
        email: {
          identifier  : 'email',
          rules: [
            {
              type   : 'empty',
              prompt : 'Please enter your e-mail'
            },
            {
              type   : 'email',
              prompt : 'Please enter a valid e-mail'
            }
          ]
        },
        password: {
          identifier  : 'password',
          rules: [
            {
              type   : 'empty',
              prompt : 'Please enter your password'
            },
            {
              type   : 'length[6]',
              prompt : 'Your password must be at least 6 characters'
            }
          ]
        },
        repassword: {
          identifier  : 'repassword',
          rules: [
            {
              type   : 'empty',
              prompt : 'Please enter your password'
            },
            {
              type   : 'length[6]',
              prompt : 'Your password must be at least 6 characters'
            }
          ]
        }
      }
    })
  ;

  $(".owl-carousel").owlCarousel({
    items: 1,
    loop:true,
    autoplay:true,
    autoplayTimeout:3000,
    autoplayHoverPause:true,
    animateOut: 'fadeOut'
  });
});