'use strict';

// modules
var needleartApp = angular.module('needleartApp', [
    'ngRoute',
    'ngAnimate'
]);

// initialization, config and routing

// configure our routes
needleartApp.config(function($routeProvider, $locationProvider) {
		$locationProvider.html5Mode({
		  enabled: true,
		  requireBase: true
		});
    $routeProvider

        // route for the home page
        .when('/', {
            templateUrl : 'app/views/home.html',
            controller  : 'homeController'
        })

        // route for the products page
        .when('/products', {
            templateUrl : 'app/views/products.html',
            controller  : 'productsController'
        })

        // route for the category page
        .when('/products/:categoryId', {
            templateUrl : 'app/views/category.html',
            controller  : 'categoryController'
        })

        // route for the product page
        .when('/products/:categoryId/:productId', {
            templateUrl : 'app/views/product.html',
            controller  : 'productController'
        })

        // route for the about page
        .when('/about', {
            templateUrl : 'app/views/about.html',
            controller  : 'aboutController'
        })

        // route for the contact page
        .when('/contact', {
            templateUrl : 'app/views/contact.html',
            controller  : 'contactController'
        })

        // 404
        .otherwise({
            templateUrl : 'app/views/notfound.html',
            controller  : 'notfoundController'
        })

});