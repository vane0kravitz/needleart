needleartApp.controller('mainController', function($scope, $rootScope, $location, $log) {
		
		//owl carousel crutch
		$scope.showCarousel = true;
		$rootScope.$on('$routeChangeSuccess', function(e, current, pre) {
	    $rootScope.path = $location.path();
	    $scope.showCarousel = ($rootScope.path != "/" || $rootScope.path != "/") ? false : true; 
	  });


    // create a message to display in our view
    $scope.general = {
    	'title': 'NeedleArt',
    	'icon': '',
    	'keywords': ''
    };

    // menu class active
    $scope.isActive = function (viewLocation) {
        return viewLocation === $location.path();
    };
});