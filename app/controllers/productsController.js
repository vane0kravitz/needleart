'use strict';

var productsController = angular.module('productsController', []);

needleartApp.controller('productsController', function($scope) {

    $scope.pageClass = 'productsView';
    $scope.products = [
    {'id': '1',
     'title': 'prod 1',
     'price': '50',
     'shortdesc': 'lorem ips 2',
     'favs': '5',
     'image': 'img/products/empty_product.png'
    	},
    {'id': '2',
     'title': 'prod 2',
     'price': '200',
     'shortdesc': 'lorem ips 2',
     'favs': '5',
     'image': 'img/products/empty_product.png'
    	},
    {'id': '3',
     'title': 'prod S',
     'price': '50',
     'shortdesc': 'lorem ips 2',
     'favs': '4',
     'image': 'img/products/empty_product.png'
    	},
    {'id': '4',
     'title': 'prod x',
     'price': '200',
     'shortdesc': 'lorem ips 2',
     'favs': '5',
     'image': 'img/products/empty_product.png'
    	},
    {'id': '5',
     'title': 'prod #',
     'price': '250',
     'shortdesc': 'lorem ips 2',
     'favs': '3',
     'image': 'img/products/empty_product.png'
    	}
    ];
    $scope.message = 'Products';
});